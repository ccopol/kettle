EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:sensors
LIBS:rambo_13
LIBS:Filament_Sensor-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CONN_01X03 P1
U 1 1 5B6DE1F9
P 1500 1800
F 0 "P1" H 1500 2000 50  0000 C CNN
F 1 "CONN_01X03" V 1600 1800 50  0001 C CNN
F 2 "Connect:PINHEAD1-3" H 1500 1800 50  0001 C CNN
F 3 "" H 1500 1800 50  0000 C CNN
	1    1500 1800
	-1   0    0    1   
$EndComp
$Comp
L C C1
U 1 1 5B6DE3B6
P 3550 1800
F 0 "C1" H 3575 1900 50  0000 L CNN
F 1 "0.47uF 50V" H 3575 1700 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 3588 1650 50  0001 C CNN
F 3 "" H 3550 1800 50  0000 C CNN
	1    3550 1800
	1    0    0    -1  
$EndComp
$Comp
L TCS40DLR U1
U 1 1 5B6DE957
P 2750 1800
F 0 "U1" H 3050 2050 60  0000 C CNN
F 1 "AH3365Q" H 3200 1650 60  0000 C CNN
F 2 "TO_SOT_Packages_SMD:SOT-23" H 2750 1800 60  0001 C CNN
F 3 "" H 2750 1800 60  0001 C CNN
	1    2750 1800
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR01
U 1 1 5B6DEDB1
P 2750 1100
F 0 "#PWR01" H 2750 950 50  0001 C CNN
F 1 "+3.3V" H 2750 1240 50  0000 C CNN
F 2 "" H 2750 1100 50  0000 C CNN
F 3 "" H 2750 1100 50  0000 C CNN
	1    2750 1100
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR02
U 1 1 5B6DEDE6
P 2750 2400
F 0 "#PWR02" H 2750 2150 50  0001 C CNN
F 1 "GND" H 2750 2250 50  0000 C CNN
F 2 "" H 2750 2400 50  0000 C CNN
F 3 "" H 2750 2400 50  0000 C CNN
	1    2750 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 1700 1850 1700
Wire Wire Line
	1850 1700 1850 1200
Wire Wire Line
	1850 1200 3550 1200
Wire Wire Line
	2750 1100 2750 1450
Connection ~ 2750 1200
Wire Wire Line
	1700 1900 1850 1900
Wire Wire Line
	1850 1900 1850 2300
Wire Wire Line
	1850 2300 3550 2300
Wire Wire Line
	2750 2150 2750 2400
Connection ~ 2750 2300
Wire Wire Line
	3550 1050 3550 1650
Wire Wire Line
	3550 1950 3550 2450
$Comp
L PWR_FLAG #FLG03
U 1 1 5B6DF099
P 3550 1050
F 0 "#FLG03" H 3550 1145 50  0001 C CNN
F 1 "PWR_FLAG" H 3550 1230 50  0000 C CNN
F 2 "" H 3550 1050 50  0000 C CNN
F 3 "" H 3550 1050 50  0000 C CNN
	1    3550 1050
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG04
U 1 1 5B6DF0B3
P 3550 2450
F 0 "#FLG04" H 3550 2545 50  0001 C CNN
F 1 "PWR_FLAG" H 3550 2630 50  0000 C CNN
F 2 "" H 3550 2450 50  0000 C CNN
F 3 "" H 3550 2450 50  0000 C CNN
	1    3550 2450
	-1   0    0    1   
$EndComp
Connection ~ 3550 1200
Connection ~ 3550 2300
Wire Wire Line
	1700 1800 2450 1800
$Comp
L CONN_01X01 MH1
U 1 1 5B6DF508
P 1050 2550
F 0 "MH1" V 1150 2550 50  0000 C CNN
F 1 "CONN_01X01" V 1150 2550 50  0001 C CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_Pad" H 1050 2550 50  0001 C CNN
F 3 "" H 1050 2550 50  0000 C CNN
	1    1050 2550
	0    -1   -1   0   
$EndComp
$Comp
L CONN_01X01 MH2
U 1 1 5B6DF54B
P 1650 2550
F 0 "MH2" V 1750 2550 50  0000 C CNN
F 1 "CONN_01X01" V 1750 2550 50  0001 C CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_Pad" H 1650 2550 50  0001 C CNN
F 3 "" H 1650 2550 50  0000 C CNN
	1    1650 2550
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR05
U 1 1 5B6DF656
P 1050 2850
F 0 "#PWR05" H 1050 2600 50  0001 C CNN
F 1 "GND" H 1050 2700 50  0000 C CNN
F 2 "" H 1050 2850 50  0000 C CNN
F 3 "" H 1050 2850 50  0000 C CNN
	1    1050 2850
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR06
U 1 1 5B6DF679
P 1650 2850
F 0 "#PWR06" H 1650 2600 50  0001 C CNN
F 1 "GND" H 1650 2700 50  0000 C CNN
F 2 "" H 1650 2850 50  0000 C CNN
F 3 "" H 1650 2850 50  0000 C CNN
	1    1650 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	1050 2750 1050 2850
Wire Wire Line
	1650 2750 1650 2850
$Comp
L R R1
U 1 1 5BA54425
P 2200 1500
F 0 "R1" V 2280 1500 50  0000 C CNN
F 1 "10k" V 2200 1500 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 2130 1500 50  0001 C CNN
F 3 "" H 2200 1500 50  0000 C CNN
	1    2200 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 1650 2200 1800
Connection ~ 2200 1800
Wire Wire Line
	2200 1350 2200 1200
Connection ~ 2200 1200
Text Notes 8150 7650 0    60   ~ 0
9-21-2018
Text Notes 7350 7500 0    60   ~ 0
TAZ7 Filament Sensor
Text Notes 8250 6950 0    118  ~ 0
FILAMENT SENSOR
Text Notes 10600 7650 0    59   ~ 0
B
Text Notes 5150 1500 0    118  ~ 0
REVISION HISTORY
Text Notes 5150 1850 0    118  ~ 0
REV. A - Initial Design  OSH Park prototype PCBs.
Text Notes 5150 2200 0    118  ~ 0
REV. B - Added R1 pull-up resistor.  Moved P1 0.5 mm away
Text Notes 6050 2400 0    118  ~ 0
from board edge for panelized assembly.  Updated
Text Notes 6050 2600 0    118  ~ 0
PCB design.
Text Notes 1300 4000 0    118  ~ 0
THEORY OF OPERATION
Text Notes 1300 4350 0    118  ~ 0
U1 is a Hall-Effect magnetic sensor that senses the poles
Text Notes 1300 4550 0    118  ~ 0
of a magnetic encoder wheel.  The encoder wheel is
Text Notes 1300 4750 0    118  ~ 0
connected to an idler wheel that runs against the
Text Notes 1300 4950 0    118  ~ 0
printer filament.  A movement of approximately 3-7 mm
Text Notes 1300 5150 0    118  ~ 0
of the filament will switch to output of the sensor.
$EndSCHEMATC
